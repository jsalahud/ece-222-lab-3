##################################################
## Name:    Lab3_Simple_Counter.s  				##
## Purpose:	Reaction Time Measurement	 		##
## Author:	Lucas Nogueira & Jawaad Salahuddin	##
##################################################

# Start of the data section
.data			
.align 4						# To make sure we start with 4 bytes aligned address (Not important for this one)
SEED:
	.word 0x1234				# Put any non zero seed
	
# The main function must be initialized in that manner in order to compile properly on the board
.text
.globl	main
main:
	# Put your initializations here
	li s1, 0x7ff60000 			# assigns s1 with the LED base address (Could be replaced with lui s1, 0x7ff60)
	li s2, 0x7ff70000 			# assigns s2 with the push buttons base address (Could be replaced with lui s2, 0x7ff70)
	li a0, 0xff					# assigns a5 with the value 225
	li t2, 0x0					# assigns t2 with the value 0
	
	COUNTER:
		addi t2, t2, 1
		sw t2, 0(s1)
		jal DELAY
		bne t2, a0, COUNTER
		li t2, 0
		j COUNTER
# End of main function		
		


# Subroutines			
DELAY:
	# Insert your code here to make a delay of a0 * 0.1 ms
	li t0, 1000					# Should delay boundary be 1000?
	mul t1, a0, t0				# a0 * 0.1 ms
	LOOP:
		addi t1, t1, -1			
		bne t1, 0, LOOP
	jr ra