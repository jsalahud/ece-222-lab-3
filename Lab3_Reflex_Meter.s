##################################################
## Name:    Lab3_Reflex_Meter.s  				##
## Purpose:	Reaction Time Measurement	 		##
## Author:	Lucas Nogueira & Jawaad Salahuddin  ##
##################################################

# Start of the data section
.data			
.align 4						# To make sure we start with 4 bytes aligned address (Not important for this one)
SEED:
	.word 0x1234				# Put any non zero seed
	
# The main function must be initialized in that manner in order to compile properly on the board
.text
.globl	main
main:
	# Put your initializations here
	li s1, 0x7ff60000 			# assigns s1 with the LED base address (Could be replaced with lui s1, 0x7ff60)
	li s2, 0x7ff70000 			# assigns s2 with the push buttons base address (Could be replaced with lui s2, 0x7ff70)
	li s3, 0					# assigns s3 with 0
	li s4, 1					# assigns s4 with 1

	sw s3, 0(s1)				# turns LED off

	jal RANDOM_NUM				#returns the pseudorandom number in a0
	jal SCALE					#returns the scaled value between 20,000 and 100,000 in a0
	jal DELAY					

	sw s4, 0(s1)				#turns LED on

	jal REFLEX_METER			#returns the reflex time in a1
	jal DISPLAY_NUM
	
# End of main function		
	


# Subroutines
RANDOM_NUM:
	# This is a provided pseudorandom number generator no need to modify it, just call it using JAL (the random number is saved at a0)
	addi sp, sp, -4				# push ra to the stack
	sw ra, 0(sp)
	
	lw t0, 0(gp)				# load the seed or the last previously generated number from the data memory to t0
	li t1, 0x8000
	and t2, t0, t1				# mask bit 16 from the seed
	li t1, 0x2000
	and t3, t0, t1				# mask bit 14 from the seed
	slli t3, t3, 2				# allign bit 14 to be at the position of bit 16
	xor t2, t2, t3				# xor bit 14 with bit 16
	li t1, 0x1000		
	and t3, t0, t1				# mask bit 13 from the seed
	slli t3, t3, 3				# allign bit 13 to be at the position of bit 16
	xor t2, t2, t3				# xor bit 13 with bit 14 and bit 16
	li t1, 0x400
	and t3, t0, t1				# mask bit 11 from the seed
	slli t3, t3, 5				# allign bit 14 to be at the position of bit 16
	xor t2, t2, t3				# xor bit 11 with bit 13, bit 14 and bit 16
	srli t2, t2, 15				# shift the xoe result to the right to be the LSB
	slli t0, t0, 1				# shift the seed to the left by 1
	or t0, t0, t2				# add the XOR result to the shifted seed 
	li t1, 0xFFFF				
	and t0, t0, t1				# clean the upper 16 bits to stay 0
	sw t0, 0(gp)				# store the generated number to the data memory to be the new seed
	mv a0, t0					# copy t0 to a0 as a0 is always the return value of any function
	
	lw ra, 0(sp)				# pop ra from the stack
	addi sp, sp, 4
	jr ra



SCALE:
	
	jr ra



DELAY:
	# Insert your code here to make a delay of a0 * 0.1 ms
	li t0, 1000					# Should delay boundary be 1000?
	mul t1, a0, t0				# a0 * 0.1 ms
	LOOP:
		addi t1, t1, -1			
		bne t1, 0, LOOP
	jr ra



REFLEX_METER:
	li a0, 1					# set delay value to 0.1 ms
	li a1, 0					# set counter to 0
	li a2, 14					# values of buttons are 1,1,1,0
	addi sp, sp, -4
	sw ra, 0(sp)
	METER:
		addi, a1, a1, 1			# increment counter by 1
		jal DELAY				# dealy of 0.1 ms
		lw t1, 0(s2)			# assign t1 with the push buttons base address
		bne t1, a2, METER		# if value at push buttons base address does not equal 1110, re-iterate loop
	lw ra, 0(sp)
	addi sp, sp, 4
	jr ra



DISPLAY_NUM:
	# Insert your code here to display the 32 bits in a0 on the LEDs byte by byte (Least isgnificant byte first) with 2 seconds delay for each byte and 5 seconds for the last
	li a0, 20000				# set delay value to 20000 ms
	mv t2, a1
	li a3, 0					# bit which is being masked
	li a4, 3					# last bit to be masked
	DISPLAY:
		andi t3, t2, 0xFF		# bit-masking
		sw t3, 0(s1)			
		srli t2, t2, 3			# shift right by 2^3 bits
		addi a3, a3, 1			# go to next bit
		jal DELAY				# delay of 20000 ms
		bne a3, a4, DISPLAY		# if all bits have not been masked, re-iterate display
	li a0, 30000				# set an additional delay value of 30000 s
	jal DELAY					# delay of 30000 ms
	j DISPLAY_NUM				# re-iterate display_num
	jr ra



#Lab Report

# 1 - If a 32-bit register is counting user reaction time in 0.1 milliseconds increments, what is the maximum amount of time which can be stored in 8 bits, 16-bits, 24-bits and 32-bits?
# 8-bits: (2^8 - 1)*0.0001 = 0.0255 s
# 16-bits: (2^16 - 1)*0.0001 = 0.65535 s
# 24-bits: (2^24 - 1)*0.0001 = 1677.7215 s
# 32-bits: (2^32 - 1)*0.0001 = 429496.7295 s

# 2 - Considering typical human reaction time, which size would be the best for this task (8, 16, 24, or 32 bits)?
# 16-bits is the smallest size that stores a value that is slower than human reaction time. Therefore, size of 16-bits would be the best for this task.